from django.db import models
from django import forms
from .models import ModelForm

import datetime

class Formulir(forms.ModelForm) :
    hari = forms.CharField(widget=forms.TextInput(
        attrs = {
            "placeholder" : 'Day'
        }
    ))
    tanggal = forms.DateField(widget=forms.DateInput(
        attrs = {
            "placeholder" : 'YYYY-MM-DD'
        }
    ))
    jam = forms.TimeField(widget=forms.TimeInput(
        attrs = {
            "placeholder" : 'Hour:Minute'
        }
    ))
    nama_kegiatan = forms.CharField(widget=forms.TextInput(
        attrs = {
            "placeholder" : 'Kegiatan'
        }
    ))
    tempat = forms.CharField(widget=forms.TextInput(
        attrs = {
            "placeholder" : 'Tempat'
        }
    ))
    kategori = forms.CharField(widget=forms.TextInput(
        attrs = {
            "placeholder" : 'Kategori'
        }
    ))

    class Meta:
        model = ModelForm
        fields = ("hari", "tanggal", "jam","nama_kegiatan", "tempat", "kategori")
        labels = {
            "hari": "Day",
            "tanggal": "Tanggal Kegiatan",
            "jam": "Jam Kegiatan",
            "nama_kegiatan": "Nama Kegiatan",
            "tempat": "Tempat",
            "kategori": "Kategori"
        }
