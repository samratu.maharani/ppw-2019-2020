from django.db import models

# Create your models here.

class ModelForm(models.Model):
    hari = models.CharField(max_length=20)
    tanggal = models.DateField()
    jam = models.TimeField(auto_now_add = False)
    nama_kegiatan = models.CharField(max_length=100)
    tempat = models.CharField(max_length=100)
    kategori = models.CharField(max_length=100)
