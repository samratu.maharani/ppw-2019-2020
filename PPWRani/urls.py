# Create your views here.
from django.urls import re_path
from . import views
#url for app

urlpatterns = [
    re_path(r'^$', views.rani_story3, name='rani-story3'),
    re_path(r'^about/$', views.about, name='about'),
    re_path(r'^experiences/$', views.experiences, name='experiences'),
    re_path(r'^skills/$', views.skills, name='skills'),
    re_path(r'^contacts/$', views.contacts, name='contacts'),
    re_path(r'^cv/$', views.cv, name='cv'),

    re_path(r'^form/$', views.form, name='form'),
    re_path(r'^show/$', views.show, name='show'),
    re_path(r'^delete-activity/(?P<pk>\d+)/$', views.delete_activity, name='delete_activity'),

]
