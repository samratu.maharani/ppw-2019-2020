from django.shortcuts import render

from .forms import Formulir
from .models import ModelForm
from django.shortcuts import get_object_or_404, redirect
from django.contrib import messages


# Create your views here.
def rani_story3(request):
    return render(request, 'rani-story3.html')

def about(request):
    return render(request, 'about.html')

def experiences(request):
    return render(request, 'experiences.html')

def skills(request):
    return render(request, 'skills.html')

def contacts(request):
    return render(request, 'contacts.html')

def cv(request):
    return render(request, 'cv.html')

def show(request):
    data = ModelForm.objects.all()
    return render(request, 'hasil.html', {'data':data})

def form(request):
    if request.method == "POST":
        data = Formulir(request.POST)
        if data.is_valid():
            data.save()
        else:
            messages.warning(request, 'Data input is not valid, Please try again!')

    return render(request, 'formulir.html', {'form': Formulir()})

def delete_activity(request, pk):
    activity = ModelForm.objects.filter(pk=pk)[0]
    activity.delete()

    # form = Formulir(instance=activity)

    # context = {
    #     'form' : form,
    #     'activity' : activity,
    # }
    return render (request, 'formulir.html')

