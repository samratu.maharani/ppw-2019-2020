from django.contrib import admin
from .models import ModelForm

admin.site.register(ModelForm)

# Register your models here.
